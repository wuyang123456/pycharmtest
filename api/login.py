#
# # 发送请求
# respone = requests.get(url="http://kdtx-test.itheima.net/api/captchaImage")
# # 发送请求
# url="http://kdtx-test.itheima.net/api/login"

# 接口封装时，重点是依据接口iu文档封装接口信息，
# 导包
import requests

# 创建对应接口的类，
class LoginAPI:
    # 初始化
    def __init__(self):
        self.url_verify = "http://kdtx-test.itheima.net/api/captchaImage"
        self.url_login = "http://kdtx-test.itheima.net/api/login"
    # 验证码
    def get_verify_code(self):
        return requests.get(url= self.url_verify)
            # pass
    # 登录
    def login(self,test_data):
        return requests.post(url=self.url_login,json=test_data)
            # pass